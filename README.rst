========
hdrstats
========


.. image:: https://img.shields.io/pypi/v/hdrstats.svg
        :target: https://pypi.python.org/pypi/hdrstats

.. image:: https://readthedocs.org/projects/hdrstats/badge/?version=latest
        :target: https://hdrstats.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




image base calculations and statistical analysis


* Free software: Mozilla Public License 2.0 (MPL 2.0)
* Documentation: https://hdrstats.readthedocs.io.


Features
--------

* TODO

Licence
-------

| Copyright (c) 2019 Stephen Wasilewski
| This Source Code Form is subject to the terms of the Mozilla Public
| License, v. 2.0. If a copy of the MPL was not distributed with this
| file, You can obtain one at http://mozilla.org/MPL/2.0/.

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage

