hdrstats (|release|)
======================================

.. toctree::

   hdrstats.hdrstats
   hdrstats.image
   cli

.. include:: ../README.rst

.. toctree::
   :maxdepth: 3
   :hidden:

   history
   genindex
   search
   todo
